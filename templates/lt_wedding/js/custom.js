/* 
Counter Up
*/
jQuery(function($){
    $('.counter').counterUp({
        delay: 100,
        time: 800
    });

    var frame = $(".poweriframe");
    console.debug(frame);
    frame.load(function() {
        console.log("TEST");
        console.debug(frame.contents().find(".powrMark"));
    });

    $("#submit-attend").click(function() {
        submitResponse(1);
        $("#submit-attend").setStyle("margin-left", '50px')
    });

    $("#submit-decline").click(function() {
        submitResponse(0);
    });

    var image = $('#fidget-loader');
    image.hide();
    console.debug(image);

    function submitResponse(response) {
        var name = $('#full_name').val();
        var email = $('#email').val();
        var msisdn = $('#msisdn').val();
        var attending = $('#attending').val();
        image.show();
        $.ajax({
            type: "POST",
            url: "index.php?option=com_ajax&plugin=rsvp&format=json",
            data: {
                name: name,
                email: email,
                msisdn: msisdn,
                attending: attending,
                accept: response
            },
            success: function(res) {
                image.attr("src","images/tick.png");
                console.debug(res, name, email, msisdn, attending);
            }
        });
    }
});

/*
WOW plugin triggers animate.css on scroll
*/
var wow = new WOW(
        {
            boxClass: 'wow', // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 100, // distance to the element when triggering the animation (default is 0)
            mobile: false        // trigger animations on mobile devices (true is default)
        }
);
wow.init();