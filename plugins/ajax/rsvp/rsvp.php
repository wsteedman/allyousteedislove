<?php
/**
* @package Helix3 Framework
* @author JoomShaper http://www.joomshaper.com
* @copyright Copyright (c) 2010 - 2015 JoomShaper
* @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or Later
*/  

//no direct accees
defined ('_JEXEC') or die ('resticted aceess');

jimport('joomla.plugin.plugin');


class plgAjaxRsvp extends JPlugin
{

    function onAjaxRsvp()
    {
        $input = JFactory::getApplication()->input;

        $name = $input->post->get('name', null, 'raw');
        $email = $input->post->get('email', null, 'raw');
        $cell = $input->post->get('msisdn', null, 'raw');
        $attending = $input->post->get('attending', null, 'raw');
        $accept = $input->post->get('accept', null, 'raw');

        $subject = $accept ? ($name . ' ACCEPTING - ' . $attending) : ($name . ' DECLINING');

        $message = $accept ? <<<TEXT
$name RSVP'd YES with $attending people attending.
Cell: $cell
Email: $email
TEXT
 : <<<TEXT
$name RSVP'd NO
Cell: $cell
Email: $email
TEXT;
        $mailer = JFactory::getMailer();

        $mail = $mailer->sendMail(
                $email, $name, 'allyousteedislove@gmail.com', $subject, $message
        );

        $db = JFactory::getDbo();
        $name = $db->q($name);
        $email = $db->q($email);
        $cell = $db->q($cell);
        $attending = $db->q($attending);

        $sql = <<<SQL
INSERT INTO steed_rsvp
(`name`,`email`,`msisdn`,`attending`,`accepted`) VALUES
($name, $email, $cell, $attending, $accept)
SQL;

        $db->setQuery($sql)->execute();

        return json_encode(['success' => $mail]);
    }

}