<?php
/*------------------------------------------------------------------------
# mod_wedding_countdown
# ------------------------------------------------------------------------
# author Your Wedding Pro
# copyright Copyright (C) 2013 http://www.yourweddingpro.co.uk. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.yourweddingpro.co.uk
# Technical Support: http://www.yourweddingpro.co.uk
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
?>

<div class="weddingcountdown">
<ul>
<?php if ($params->get('showbride')=="1") : ?>
<li class="<?php echo $params->get('liststyle'); ?> bride"><strong>Bride:</strong> <?php echo $params->get('bride'); ?></li>
<?php endif; ?>
<?php if ($params->get('showgroom')=="1") : ?>
<li class="<?php echo $params->get('liststyle'); ?> groom"><strong>Groom:</strong> <?php echo $params->get('groom'); ?></li>
<?php endif; ?>
<?php if ($params->get('showvenue')=="1") : ?>
<li class="<?php echo $params->get('liststyle'); ?> venue"><strong>Venue:</strong> <?php echo $params->get('venue'); ?></li>
<?php endif; ?>
<?php if ($params->get('showdate')=="1") : ?>
<li class="<?php echo $params->get('liststyle'); ?> date"><strong>Date:</strong> <?php echo $params->get('day'); ?> <?php echo $params->get('month'); ?> <?php echo $params->get('year'); ?></li>
<?php endif; ?>
<?php if ($params->get('showcountdown')=="1") : ?>
<li class="<?php echo $params->get('liststyle'); ?> countdown"><span id="countdown1"><?php echo $params->get('calendar'); ?> <?php echo $params->get('hour'); ?>:<?php echo $params->get('minute'); ?>:00 GMT<?php echo $params->get('timediff'); ?>:00</span></li>
<?php endif; ?>
</ul>
<p class="powered">Powered by <a href="http://www.yourweddingpro.co.uk" title="Your Wedding Pro">Your Wedding Pro</a></p>
</div>