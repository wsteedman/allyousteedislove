
<?php
/*------------------------------------------------------------------------
# mod_wedding_countdown
# ------------------------------------------------------------------------
# author Your Wedding Pro
# copyright Copyright (C) 2013 http://www.yourweddingpro.co.uk. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.yourweddingpro.co.uk
# Technical Support: http://www.yourweddingpro.co.uk
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$document = &JFactory::getDocument();
$document->addStyleSheet( JURI::base() . 'modules/mod_wedding_countdown/css/default.css' );
$document->addScript( JURI::base() . 'modules/mod_wedding_countdown/js/weddingcountdown.js' );




require JModuleHelper::getLayoutPath('mod_wedding_countdown', $params->get('layout', 'default'));
